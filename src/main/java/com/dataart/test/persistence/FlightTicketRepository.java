package com.dataart.test.persistence;

import com.dataart.test.model.FlightTicketEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlightTicketRepository extends JpaRepository<FlightTicketEntity, Long> {
}
