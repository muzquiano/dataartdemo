package com.dataart.test.service;

import com.dataart.test.model.FlightTicketEntity;
import com.dataart.test.model.FlightTicketNotFoundException;

import javax.transaction.Transactional;
import java.util.List;

public interface FlightTicketService {

    FlightTicketEntity createFlightTicket(FlightTicketEntity ticket);

    FlightTicketEntity getFlightTicketByItinerary(Long id);

    List<FlightTicketEntity> getAllFlightTickets();
}
