package com.dataart.test.service;

import com.dataart.test.model.FlightTicketEntity;
import com.dataart.test.model.FlightTicketNotFoundException;
import com.dataart.test.persistence.FlightTicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class FlightTicketServiceImpl implements FlightTicketService {

    @Autowired
    private FlightTicketRepository repo;

    @Transactional
    public FlightTicketEntity createFlightTicket(FlightTicketEntity ticket) {
        return repo.save(ticket);
    }

    @Transactional
    public FlightTicketEntity getFlightTicketByItinerary(Long id) {
        return repo.findById(id).orElseThrow(() -> new FlightTicketNotFoundException(id));
    }

    @Transactional
    public List<FlightTicketEntity> getAllFlightTickets() {
        return repo.findAll();
    }
}
