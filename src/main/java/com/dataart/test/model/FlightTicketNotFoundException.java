package com.dataart.test.model;

public class FlightTicketNotFoundException extends RuntimeException {

    public FlightTicketNotFoundException(Long id) {
        super(String.format("Could not find flight ticket for ID %s ", id));
    }
}
