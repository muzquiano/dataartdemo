package com.dataart.test.controller;

import com.dataart.test.model.FlightTicketEntity;
import com.dataart.test.service.FlightTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path="/ticket", produces="application/json")
public class FlightTicketController {

    @Autowired
    private FlightTicketService service;

    @GetMapping("/all")
    public List<FlightTicketEntity> getAll() {
        return service.getAllFlightTickets();
    }

    @GetMapping("/{itineraryId}")
    public FlightTicketEntity geFlightTicketById(@PathVariable("itineraryId") Long itineraryId) {
        return service.getFlightTicketByItinerary(itineraryId);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public FlightTicketEntity createFlightTicket(@Valid @RequestBody FlightTicketEntity newTicket) {
        return service.createFlightTicket(newTicket);
    }
}
