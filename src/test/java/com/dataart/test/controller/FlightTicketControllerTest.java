package com.dataart.test.controller;

import com.dataart.test.model.FlightTicketEntity;
import com.dataart.test.model.FlightTicketNotFoundException;
import com.dataart.test.service.FlightTicketServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(FlightTicketController.class)
public class FlightTicketControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FlightTicketServiceImpl service;

    @Test
    public void getAllTicketsTest() throws Exception {

        FlightTicketEntity ticket = new FlightTicketEntity();
        ticket.setPassengerName("passengerName");
        List<FlightTicketEntity> allTickets = Arrays.asList(ticket);

        given(service.getAllFlightTickets()).willReturn(allTickets);

        mvc.perform(MockMvcRequestBuilders
                .get("/ticket/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].passengerName", is(ticket.getPassengerName())));
    }

    @Test
    public void geFlightTicketByIdTest() throws Exception {

        FlightTicketEntity ticket = new FlightTicketEntity();
        ticket.setId(Long.valueOf(10));
        ticket.setPassengerName("passengerName");

        given(service.getFlightTicketByItinerary(Long.valueOf(10)))
                .willReturn(ticket);

        mvc.perform(MockMvcRequestBuilders
                .get("/ticket/{itineraryId}", 10)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(10)))
                .andExpect(jsonPath("$.passengerName", is(ticket.getPassengerName())));
    }

    @Test
    public void geFlightTicketById_NotFound_Test() throws Exception {

        given(service.getFlightTicketByItinerary(Long.valueOf(10)))
                .willThrow(new FlightTicketNotFoundException(Long.valueOf(10)));

        mvc.perform(MockMvcRequestBuilders
                .get("/ticket/{itineraryId}", 10)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createFlightTicketTest() throws Exception {

        FlightTicketEntity ticket = new FlightTicketEntity();
        ticket.setPassengerName("passengerName");

        given(service.createFlightTicket(ticket)).willReturn(ticket);

        mvc.perform(MockMvcRequestBuilders
                .post("/ticket")
                .content("{\"id\":null,\"departureDate\":\"2020-10-10\",\"departureTime\":\"10:10:00\",\"arrivalDate\":\"2020-10-11\",\"arrivalTime\":\"10:11:00\",\"originCity\":\"from_City\",\"destinationCity\":\"to_City\",\"passengerName\":\"passenger\",\"passengerAge\":20,\"luggageStored\":false,\"tickerPrice\":10.00}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

    }
}
