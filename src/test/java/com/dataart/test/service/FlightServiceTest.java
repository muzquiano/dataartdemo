package com.dataart.test.service;

import com.dataart.test.model.FlightTicketEntity;
import com.dataart.test.persistence.FlightTicketRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class FlightServiceTest {

    @TestConfiguration
    static class FlightTicketServiceTestContextConfiguration {

        @Bean
        public FlightTicketService flightTicketService() {
            return new FlightTicketServiceImpl();
        }
    }

    @Autowired
    private FlightTicketService service;

    @MockBean
    private FlightTicketRepository repo;

    @Before
    public void setUp() {
        FlightTicketEntity ticket = new FlightTicketEntity();
        ticket.setId(Long.valueOf(10));

        Mockito.when(repo.save(ticket))
                .thenReturn(ticket);

        Mockito.when(repo.findById(Long.valueOf(10)))
                .thenReturn(Optional.of(ticket));

        Mockito.when(repo.findAll())
                .thenReturn(Arrays.asList(ticket));
    }

    @Test
    public void createFlightTicketTest() {

        //service.createFlightTicket();
       // Assert.assertNotNull(itineraryId);
    }

    @Test
    public void getFlightTicketByIdTest() {

        FlightTicketEntity ticket = service.getFlightTicketByItinerary(Long.valueOf(10));
        Assert.assertNotNull(ticket);
        Assert.assertEquals(ticket.getId(), Long.valueOf(10));
    }
}
