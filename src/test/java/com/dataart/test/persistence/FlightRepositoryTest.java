package com.dataart.test.persistence;


import com.dataart.test.model.FlightTicketEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FlightRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private FlightTicketRepository repo;

    @Test
    public void saveAndRetrieveTicketTest() {

        FlightTicketEntity ticket = new FlightTicketEntity();
        ticket.setPassengerAge(99);
        ticket.setPassengerName("PName");
        ticket.setOriginCity("Tandil");
        ticket.setDestinationCity("MarDelPlata");
        ticket.setDepartureDate(LocalDate.of(2020, 10, 10));
        ticket.setArrivalDate(LocalDate.of(2020, 10, 10));
        ticket.setArrivalTime(LocalTime.of(10, 10));
        ticket.setDepartureTime(LocalTime.of(10, 10));
        ticket.setLuggageStorage(Boolean.FALSE);
        ticket.setTickerPrice(BigDecimal.TEN);

        ticket = entityManager.persist(ticket);
        entityManager.flush();

        Optional<FlightTicketEntity> retrievedTicketOptional = repo.findById(ticket.getId());

        Assert.assertNotNull(retrievedTicketOptional);
        Assert.assertTrue(retrievedTicketOptional.isPresent());

        FlightTicketEntity retrievedTicket = retrievedTicketOptional.get();
        Assert.assertEquals(ticket.getPassengerAge(), retrievedTicket.getPassengerAge());
        Assert.assertEquals(ticket.getPassengerName(), retrievedTicket.getPassengerName());
        Assert.assertEquals(ticket.getOriginCity(), retrievedTicket.getOriginCity());
        Assert.assertEquals(ticket.getDestinationCity(), retrievedTicket.getDestinationCity());
        Assert.assertEquals(ticket.getDepartureDate(), retrievedTicket.getDepartureDate());
        Assert.assertEquals(ticket.getArrivalDate(), retrievedTicket.getArrivalDate());
        Assert.assertEquals(ticket.getArrivalTime(), retrievedTicket.getArrivalTime());
        Assert.assertEquals(ticket.getDepartureTime(), retrievedTicket.getDepartureTime());
        Assert.assertEquals(ticket.getLuggageStorage(), retrievedTicket.getLuggageStorage());
        Assert.assertEquals(ticket.getTickerPrice().longValueExact(), retrievedTicket.getTickerPrice().longValueExact());
    }
}
