# DataArt Demo Project #

Please find step to run FlighTicket Demo service. Note: <java.version>11</java.version> used.

### Get Code and  ###

* git clone https://muzquiano@bitbucket.org/muzquiano/dataartdemo.git
* mvn clean install
* To start Spring Boot App run --> ./mvnw spring-boot:run


### Examples  ###
* curl -v -X POST localhost:8080/ticket -H "Content-type:application/json" -d "{\"departureDate\":\"2020-10-10\",\"departureTime\":\"10:10:00\",\"arrivalDate\":\"2020-10-11\",\"arrivalTime\":\"10:11:00\",\"originCity\":\"from_City\",\"destinationCity\":\"to_City\",\"passengerName\":\"passenger\",\"passengerAge\":20,\"luggageStorage\":true,\"tickerPrice\":10.00}"
* http://local:8080/ticket/all to see all created Flight Tickets.
* http://local:8080/ticket/{ID} to see an specific Flight Ticket.

### JSON example ###

{
	"departureDate":"2020-10-10",
	"departureTime":"10:10:00",
	"arrivalDate":"2020-10-11",
	"arrivalTime":"10:11:00",
	"originCity":"from_City",
	"destinationCity":"to_City",
	"passengerName":"passenger",
	"passengerAge":20,
	"luggageStorage":true,
	"tickerPrice":10.00
}